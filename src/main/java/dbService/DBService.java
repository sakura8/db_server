package dbService;

import dbService.dao.Request;
import metrics.Metrics;
import metrics.MetricsData;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author v.chibrikov
 *         <p>
 *         Пример кода для курса на https://stepic.org/
 *         <p>
 *         Описание курса и лицензия: https://github.com/vitaly-chibrikov/stepic_java_webserver
 */
public class DBService <V> implements IMetricsVisitor{
    private final Connection connection;

    public DBService() {
        this.connection = getRedConnection();
    }

    public void close () {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void visit(Metrics metrics, String schema_name, String table_name) throws DBException, ColumnException {

        try {
            String query = metrics.getQuery(schema_name, table_name);
            Request request = new Request(connection, query);
            MetricsData metricsData = request.execute();
            metrics.setData(metricsData);

        } catch (SQLException e) {
            throw new DBException(e);
        }

    }

    public void printConnectInfo() {
        try {
            System.out.println("DB name: " + connection.getMetaData().getDatabaseProductName());
            System.out.println("DB version: " + connection.getMetaData().getDatabaseProductVersion());
            System.out.println("Driver: " + connection.getMetaData().getDriverName());
            System.out.println("Autocommit: " + connection.getAutoCommit());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("UnusedDeclaration")

    public static Connection getRedConnection() {
        try {
            Driver driver = (Driver) Class.forName("org.postgresql.Driver").newInstance();
            StringBuilder url = new StringBuilder();
            PropertiesContainer prop = new PropertiesContainer();
            url.
                    append("jdbc:postgresql://").    //db postgressql
                    append("fls-playground.cq2hhifgzij7.us-west-2.redshift.amazonaws.com:"). //host name
                    append("5439/").                //port
                    append("flsdb");          //db name

            //System.out.println("URL: " + url + "\n");

            String name = "flsuser";
            String pass = "ChuckNorr1s";

           /*url.
                    append("jdbc:postgresql://").    //db postgressql
                    append(prop.getPropValues().getProperty("hostname")).   //host name
                    append(prop.getPropValues().getProperty("port")).                //port
                    append(prop.getPropValues().getProperty("dbname"));          //db name    */

            //System.out.println("URL: " + url + "\n");

            /*String name = prop.getPropValues().getProperty("username");
            String pass = prop.getPropValues().getProperty("password");  */

            Connection connection = DriverManager.getConnection(url.toString(), name, pass);

            return connection;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InstantiationException e)  {
            e.printStackTrace();
        }  catch (IllegalAccessException e)  {
            e.printStackTrace();
        }  catch (ClassNotFoundException e)  {
            e.printStackTrace();
        }
        return null;
    }
}
