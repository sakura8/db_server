package metrics;

import java.io.*;

/**
 * Created by IPermyakova on 06.07.2016.
 */
public abstract class Metrics implements Serializable {
    protected String col_name;
    protected Boolean isEmpty;
    protected String sql_format;
    protected MetricsData data;


    public Metrics(String col_name) {
        this.col_name = col_name;
    }

    public Metrics(){};

    public String getQuery(String schema_name, String table_name)
    {
        return String.format(sql_format, schema_name, table_name, col_name);
    }

    public MetricsData getData(){return data;};

    public void setData(MetricsData data){
        this.data = data;
        this.isEmpty = false;
    };

    public abstract String getInfo();

    public  Boolean getExecute() {
        return isEmpty;
    };

    /*
    public static Object loadToStream(String fileName) throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(fileName);
        ObjectInputStream ois = new ObjectInputStream(fis);
        Object obj = ois.readObject();
        ois.close();
        return obj;
    };

    public static void loadFromStream(String fileName) throws IOException {
        FileOutputStream fos = new FileOutputStream(fileName);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(this);
        fos.close();
    };    */


    public static ObjectOutputStream loadToStream(Metrics obj) throws IOException, ClassNotFoundException {
        //return new ObjectOutputStream().writeObject(obj);
        return null;
    };

    public static Metrics loadFromStream(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        return (Metrics) ois.readObject();
    };


}
