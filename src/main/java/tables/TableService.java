package tables;


import dbService.DBService;

/**
 * Created by IPermyakova on 28.06.2016.
 */
public class TableService {
    DBService dbService = new DBService();

    public TableService() {
        dbService.printConnectInfo();
    }

    /*public long getDistinctCountMain(String tableName) throws DBException {
        DTO count = dbService.getDistinctCountMain(tableName);
        return count;
    }

    public List<String> getColumns(String tableName) throws DBException {
        List<String> columns = dbService.getColumns(tableName);
        return columns;
    }

    public List<String> getColumnsWithoutOneValues(String tableName, Map<String, String> inputMap) throws DBException {
        List<String> columns = dbService.getColumns(tableName);
        columns.removeAll(inputMap.keySet());
        return columns;
    }

    public Map<String, String> getColumnsWithOneValues(List<String> clm, String tableName) throws DBException {
        Map<String, Long> testMap = dbService.getDistinctCount(clm, tableName);
        Map<String, String> resultMap = new HashMap<String, String>();

        for (String s : testMap.keySet()) {
            if (testMap.get(s) == 0 | testMap.get(s) == 1) {     //one value or null
                List<String> values = dbService.getDistinctValuesForField(s, tableName);
                if (values.size() == 1) {
                    resultMap.put(s, values.get(0));
                }
            }
        }
        return resultMap;
    }

    public Map<String, Long> getDistinctCount(List<String> lst, String tableName) throws DBException {
        return dbService.getDistinctCount(lst, tableName);
    }

    public Map<String, Long> getNullCount(List<String> lst, String tableName) throws DBException {
        return dbService.getNullCount(lst, tableName);
    }

    public Map<String, List<String>> getTopValues(List<String> lst, String tableName) throws DBException {
        return dbService.getTopValues(lst, tableName);
    }

   /* public List<String> getTables() throws DBException {
        return dbService.getTables();

    }    */

}