package main;


import dbService.DBService;
import metrics.CmetricRowsForTable;
import metrics.Metrics;

/**
 * @author v.chibrikov
 *         <p>
 *         Пример кода для курса на https://stepic.org/
 *         <p>
 *         Описание курса и лицензия: https://github.com/vitaly-chibrikov/stepic_java_webserver
 */
public class Main {


    public static void main (String[] arg) throws Exception {
        DBService dbService = new DBService();
        String schemaName = "flsdb.princeton_plainsboro_etl";
       String tableName = "person_f";
       String col_name = "person_id";
        //String tableName = "visit_occurrence_f";
        //String col_name = "visit_start_date";

        /*Check*/
        Metrics metric = new CmetricRowsForTable();
       // Metrics metric = new CmetricUniqueValuesForTable();
         //Metrics metric = new CmetricUniqueValues(col_name);
        //Metrics metric = new CmetricNulls(col_name);

        //Metrics metric = new CmetricList(col_name);
        String Sql_query = metric.getQuery("flsdb.princeton_plainsboro_etl", tableName);
        dbService.visit(metric, schemaName, tableName);
        metric.getData();
        System.out.println("This is resulting query: " + Sql_query);
        System.out.println(metric.getData());

        System.out.println(metric.getExecute());

    }

    public static void printUsage() {
        String message = "\n\r" + "\n\r" +  "Usage:  java -jar server.jar made";
        System.out.println(message);
    }

}
